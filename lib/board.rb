require 'byebug'

class Board
  attr_reader :grid, :winner

  def initialize(grid = [])
    3.times { grid.push(Array.new(3)) } if grid.empty?
    @grid = grid
    @winner = nil
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def [](pos)
    row, column = pos
    @grid[row][column]
  end

  def []=(pos, mark)
    row, column = pos
    @grid[row][column] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    self.row_check
    self.column_check
    self.diagonal_check
    @winner
  end

  def over?
    if self.winner || @grid.all? { |row| !row.include?(nil) }
      return true
    end
    false
  end

  def row_check
    @grid.each do |row|
      if row.all? { |mark| mark == row[0] }
        @winner = row[0] if row[0]
      end
    end
  end

  def column_check
    grid_dup = @grid.dup
    self.rotate

    self.row_check

    @grid = grid_dup
  end

  def diagonal_check
    diagonal = [self[[0, 0]], self[[1, 1]], self[[2, 2]]]
    if diagonal.all? { |mark| mark == diagonal[0] }
      @winner = diagonal[0] if diagonal[0]
    end
    diagonal = [self[[2, 0]], self[[1, 1]], self[[0, 2]]]
    if diagonal.all? { |mark| mark == diagonal[0] }
      @winner = diagonal[0] if diagonal[0]
    end
  end

  def rotate
    rotated = []
    i = 0
    while i < 3
      new_row = []
      grid.each { |row| new_row.unshift(row[i]) }
      rotated.push(new_row)
      i += 1
    end
    @grid = rotated
  end
end
