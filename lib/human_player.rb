class HumanPlayer
  attr_reader :name, :move
  
  def initialize(name)
    @name = name
    @move = []
  end

  def get_move
    puts 'Move where?:'
    @move = gets.chomp.split(', ').map(&:to_i)
  end

  def display(board)
    puts board.grid
  end
end
