require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board, :current_player

  def initialize(player_one, player_two)
    @current_player = player_one
    @dormant_player = player_two
    @board = Board.new
  end

  def play_turn
    @current_player.display(@board)
    @dormant_player.display(@board)

    # while !board.over?
    move = @current_player.get_move
    board.place_mark(move, @current_player.mark)

    self.switch_players!
    # end
  end

  def switch_players!
    @current_player, @dormant_player = @dormant_player, @current_player
  end
end
