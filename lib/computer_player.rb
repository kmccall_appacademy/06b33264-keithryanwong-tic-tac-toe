class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = []
    (0..2).each do |i|
      (0..2).each do |j|
        moves.push [i, j] if board[[i, j]] == nil
      end
    end

    moves.each do |pos|
      @board.place_mark(pos, @mark)
      if @board.winner == @mark
        @board.place_mark(pos, nil)
        return pos
      end
      @board.place_mark(pos, nil)
    end

    moves.sample
  end
end
